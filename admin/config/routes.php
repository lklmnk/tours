<?php
return array(
    "admin"          => "home/index",
    "admin/login"    => "home/login",
    "admin/logout"   => "home/logout",
    "admin/page404"   => "home/page404",
    "admin/categories"    => "category/list",
    "admin/category/edit/([0-9]+)" => "category/edit/$1",
    "admin/category/create"        => "category/create",
    "admin/category/([0-9]+)"      => "category/view/$1",
    "admin/category/submit"         => "category/submit",
    
    "admin/tours"              => "tour/list",
    "admin/tours/([0-9]+)"     => "tour/list/$1",
    "admin/tour/edit/([0-9]+)" => "tour/edit/$1",
    "admin/tour/copy/([0-9]+)" => "tour/copy/$1",
    "admin/tour/create"        => "tour/create",
    "admin/tour/([0-9]+)"      => "tour/view/$1",
    "admin/tour/submit"         => "tour/submit",
    "admin/tour/image/([0-9]+)"  => "tour/image/$1",
    "admin/image/submit"         => "image/submit",
);
