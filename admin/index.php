<?php

    define("ROOT", __DIR__);
    
    include("../config/config.php");
    
    spl_autoload_register(function($className){
        $dirs = [
            "../core/",
            ROOT . "/modules/controllers/",
            ROOT . "/modules/models/",
        ];
        foreach($dirs as $dir) {
            if (is_file($dir . $className . ".php")) {
                include $dir . $className . ".php";
            }
        }
    });
    
    DataBase::Connect();
    BackendController::Init();
    BackendController::Route();
    BackendController::Action();
    BackendController::Display();