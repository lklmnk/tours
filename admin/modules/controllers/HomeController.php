<?php
class HomeController extends BackendController
{
    const ADMIN_USER = "admin";
    const ADMIN_PASSWORD = "1";
    
    public function IndexAction($parameters) {
        header("Location: /admin/login");
        die;
    }
    
    public function LoginAction($parameters) {
        if (isset($_SESSION["admin"]) && $_SESSION['admin'] == true) {
            header("Location: /admin/tours");
            die;
        }
        
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            if ($_POST["login"] == self::ADMIN_USER && 
                $_POST["password"] == self::ADMIN_PASSWORD) {
                    session_start();
                    $_SESSION["admin"] = true;
                    header("Location: /admin/tours");
                    die;
                } else {
                    self::$logs['errors'][] = "Неуспішний вхід. Спробуйте ще раз.";
                }
        }
        $content = View::Getcontents(ROOT."/modules/views/home/login_form.tpl");
        self::$mainView->addParam("title", "Форма для входу");
        self::$mainView->addParam("content", $content);
    }
    
    public function LogoutAction($parameters) {
        session_destroy();
        header("Location: /admin/login");
        die;
    }
    
    public function Page404Action($parameters){
        self::$mainView->addParam("title", "Сторінка 404");
        self::$mainView->addParam("content", "Тут буде шаблон сторінки 404");
    }
}
