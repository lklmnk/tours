<?php

class ImageController extends BackendController {

    public function SubmitAction($parameters) {
        print_r($_FILES);
        $image_id=0;
        if (isset($_POST['action']) && $_POST['action'] == 'edit') {
            $new_category = new CategoryModel("categories", "category");
            $new_category->initObjectFromArray($_POST);
            $category_id = $new_category->update("categories","category_id");
        } elseif (isset($_POST['action']) && $_POST['action'] == 'create') {
            $new_img = new Images("images", "image");
            $new_img->initObjectFromArray($_POST);

           // $cztegory_id = $new_img->insert("images");
        } elseif (isset($_POST['action']) && $_POST['action'] == 'active') {
            CategoryModel::changeActiveStatus($_POST);
            header("Location: /admin/categories");
            die;
        }

        header("Location: /admin/category/$category_id");
        die;
    }

    public function ViewAction($parameters)
    {
        $category_id = $parameters[0];
        $category = new Category("categories","category",$category_id);
        $params["category"] = $category;
        $content = View::GetContents(ROOT."/modules/views/category/view.tpl", $params);
        self::$mainView->addParam("title", $category->category_name);
        self::$mainView->addParam("content", $content);
    }



    public function EditAction($parameters)
    {
        $category_id = $parameters[0];
        $category = new Category("categories","category",$category_id);
        $categories = CategoryModel::getModels("categories","category");
        $params["category"] = $category;
        $params["categories"] = $categories;
        $params["action"] = "edit";
        $content = View::GetContents(ROOT . "/modules/views/category/form.tpl", $params);
        self::$mainView->addParam("category_id", $category->category_id);
        self::$mainView->addParam("title", "Редагування категорії");
        self::$mainView->addParam("content", $content);
        self::$mainView->addParam("categories", $categories);
    }

    public function CreateAction($parameters)
    {
        $categories =  CategoryModel::getModels("categories","category");
        $params["categories"] = $categories;
        $params["action"] = "create";
        $content = View::GetContents(ROOT . "/modules/views/categorys/form.tpl", $params);
        self::$mainView->addParam("categories", $categories);
        self::$mainView->addParam("title", "Створення статті");
        self::$mainView->addParam("Content", $content);
    }

    public function ListAction($parameters)
    {
        $categories =  CategoryModel::getModels("categories","category");
        $params["categories"] = $categories;
        $content = View::GetContents(ROOT."/modules/views/category/list.tpl", $params);
        self::$mainView->addParam("title", "Список категорій");
        self::$mainView->addParam("content", $content);
        self::$mainView->addParam("categories", $categories);
    }
}
