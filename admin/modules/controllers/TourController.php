<?php

class TourController extends BackendController
{

    public function SubmitAction($parameters) {
        $tour_id = 0;
        if (isset($_POST['action']) && $_POST['action'] == 'edit') {
            if(!empty($_FILES)) Images::writeFile($_FILES,$_POST);
            $new_tour = new TourModel("tours", "tour");
            $new_tour->initObjectFromArray($_POST);
            $tour_id = $new_tour->update("tours","tour_id");
        } elseif (isset($_POST['action']) && $_POST['action'] == 'create') {
            $new_tour = new TourModel("tours", "tour");
            $new_tour->initObjectFromArray($_POST);
            $tour_id = $new_tour->insert("tours");
        }
        elseif (isset($_POST['action']) && $_POST['action'] == 'copy') {
            $new_tour = new TourModel("tours", "tour");
            array_shift($_POST);
            $new_tour->initObjectFromArray($_POST);
            $tour_id = $new_tour->insert("tours");

        }elseif (isset($_POST['action']) && $_POST['action'] == 'active') {
            TourModel::changeActiveStatus($_POST);
            header("Location: /admin/tours");
            die;
        }

        header("Location: /admin/tour/$tour_id");
        die;
    }
    public function ViewAction($parameters)
    {
        $tour_id = $parameters[0];
        $tour = new TourModel('tours', 'tour', $tour_id);
        $params["tour"] = $tour;
        $content = View::GetContents(ROOT."/modules/views/tour/view.tpl", $params);
        self::$mainView->addParam("title", $tour->tour_name);
        self::$mainView->addParam("content", $content);

    }
    public function EditAction($parameters)
    {
        $tour_id = $parameters[0];
        $tour = new TourModel("tours", "tour", $tour_id);
        $params["tour"] = $tour;
        $params["action"] = 'edit';
        $categories = CategoryModel::getModels("categories","category");
        $params["categories"]=$categories;
        $image = new Images($tour_id,$param='',$tour_id);
        $params["image"] = $image;
        $content = View::GetContents(ROOT."/modules/views/tour/form.tpl", $params);
        self::$mainView->addParam("title", $tour->tour_name);
        self::$mainView->addParam("content", $content);
        self::$mainView->addParam("tour_id", $tour_id);
    }




    public function CreateAction($parameters)
    {
        $tour = new TourModel('tours', 'tour');
        $params["tour"] = $tour;
        $categories = CategoryModel::getModels("categories","category");
        $params["categories"]=$categories;
        $params["action"] = "create";
        $image = new Images(0,$param='',0);
        $params["image"] = $image;
        $content = View::Getcontents(ROOT . "/modules/views/tour/form.tpl", $params);
        self::$mainView->addParam("title", "Створення туру");
        self::$mainView->addParam("content", $content);
        self::$mainView->addParam("action", $params["action"]);
    }
    public function CopyAction($parameters)
    {
        $tour_id = $parameters[0];
        $tour = new TourModel("tours", "tour", $tour_id);
        $params["tour"] = $tour;
        $params["action"] = 'copy';
        $categories = CategoryModel::getModels("categories","category");
        $params["categories"]=$categories;
        $image = new Images($tour_id,$param='',$tour_id);
        $params["image"] = $image;
        $content = View::GetContents(ROOT."/modules/views/tour/form.tpl", $params);
        self::$mainView->addParam("title", $tour->tour_name);
        self::$mainView->addParam("content", $content);
        self::$mainView->addParam("tour_id", $tour_id);
    }

    public function ListAction($parameters = [])
    {
        $category_id = (isset($parameters[0]))?$parameters[0]:0;
        $tours = TourModel::getModels("tours","category",$category_id);
        $params["tours"] = $tours;
        $categories = CategoryModel::getModels("categories","category");
        $content = View::Getcontents(ROOT . "/modules/views/tour/list.tpl", $params);
        self::$mainView->addParam("title", "Список турів");
        self::$mainView->addParam("content", $content);
        self::$mainView->addParam("categories", $categories);
    }
    public function ImageAction($parameters)
    {
        $tour_id = $parameters[0];
        $image = new Images($tour_id,$param='',$tour_id);
        $params["image"] = $image;
        $params["action"] = 'create';
        $tours = TourModel::getModels("tours","tour");
        $params["tours"]=$tours;
        $content = View::GetContents(ROOT."/modules/views/image/form.tpl", $params);
        self::$mainView->addParam("title","Завантаження зображення");
        self::$mainView->addParam("content", $content);
        self::$mainView->addParam("tour_id", $tour_id);

    }

}
