<form method='post' action='/admin/category/submit'>
    <div>
        <input type='text' readonly="readonly" name='category_id' value='<?php echo $category->category_id; ?>'>
    </div>
    <div>
        <input type='text' name='parent_id' value='<?php echo $category->parent_id; ?>'>
    </div>
    <div>
    <input type='text' name='category_name' value='<?php echo $category->category_name; ?>'>
    </div>
    
    <div>
    <textarea name='category_description'><?php echo $category->category_description; ?></textarea>
    </div>
    <div>
        <?php if ($category->is_active): ?>
        <input type='checkbox' name='is_active' value="1" checked="checked">
        <?php else: ?>
        <input type='checkbox' name='is_active' value="1">
        <?php endif; ?>
    </div>
    <div>
    <input type='text' name='category_url' value='<?php echo $category->category_url; ?>'>
    </div>
    <div>
        <input type='hidden' name='action' value='<?php echo $action; ?>'>
        <input type='submit' >
    </div>
</form>