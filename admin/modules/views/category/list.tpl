<form action="/admin/category/submit" method="post">
    <?php foreach($categories as $key => $category): ?>
    <div>
        <input type="checkbox" name="category[]" value="<?php echo $category['category_id']; ?>">
        <a href="/admin/category/edit/<?php echo $category['category_id']; ?>">
            <span <?php if (!$category['is_active']): ?>style="text-decoration: line-through;"<?php endif; ?>>
            <?php echo $category['category_name']; ?>
            </span>
        </a>
    </div>
    <?php endforeach;?>
    <input type="hidden" name="action" value="active">
    <input type="submit"  value="Де/Актувати">
</form>
