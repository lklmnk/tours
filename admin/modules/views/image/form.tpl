<form method='post' action='/admin/image/submit' enctype="multipart/form-data">
    <div>
        <input type='text' readonly="readonly" name='image_id' value='<?php echo $image->image_id; ?>'>
    </div>
    <div>
    <input type='text' name='title' value='<?php echo $image->title; ?>'>
    </div>
    <div>
        <input type='text' name='gallery_id' value='<?php echo $image->gallery_id; ?>'>
    </div>
    <input type="file" multiple="multiple" name="images[]">
    <div>
    <select  name='tour_id'>
        <?php foreach($tours as $tour): ?>
           <?php if ($image->tour_id == $tour['tour_id']): ?>
           <option value="<?= $tour['tour_id']?>" selected='selected'><?= $tour['tour_name']?></option>
           <?php else: ?>
           <option  value="<?= $tour['tour_id']?>"><?= substr($tour['tour_name'],0,100); ?></option>
           <?php endif; ?>
        <?php endforeach; ?>
    </select>
    </div>
    <div><?php echo $action; ?>
        <input type='hidden' name='action' value='<?php echo $action; ?>'>
        <input type='submit'>
    </div>
</form>