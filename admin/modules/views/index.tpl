<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?php echo $title; ?></title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="/admin/modules/views/css/style.css">
    <style>
        .error {
            background-color: red;
            color: white;
        }
    </style>
</head>
<body>
<nav class="navbar navbar-default">
    <div class="container-fluid">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Brand</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <?php if ($admin): ?>
            <ul class="nav navbar-nav">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle active" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Список турів <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <?php foreach($categories as $key => $category): ?>
                        <li><a  href="/admin/tours/<?php echo $category['category_id']; ?>">
                                <span><?php echo $category["category_name"]; ?></span>
                            </a>
                        </li>
                        <?php endforeach;?>
                    </ul>
                </li>
                <li >
                    <a href="/admin/categories">Категорії турів </a>
                </li>

                </li>
                <li><a href='/admin/logout'>Вийти</a></li>
            </ul>
            <?php endif; ?>

        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>
<div class="container">

    <?php if ($logs['errors']): ?>
    <?php foreach ($logs['errors'] as $error): ?>
    <div class='alert alert-danger'><?php echo $error; ?></div>
    <?php endforeach; ?>
    <?php endif; ?>
    <?php if(isset($tour_id)): ?>
    <nav class="navbar navbar-default">
        <ul class="nav navbar-nav">
            <li><a href='/admin/tour/copy/<?php echo $tour_id; ?>'>Копіювати тур</a></li>
            <li><a href='/admin/tour/create'>Створити тур</a></li>
        </ul>
    </nav>
    <?php endif; ?>
    <?php if(isset($category_id)): ?>
    <nav class="navbar navbar-default">
        <ul class="nav navbar-nav">
            <li><a href='/admin/category/copy/<?php echo $category_id; ?>'>Копіювати категорію</a></li>
            <li><a href='/admin/category/create'>Створити категорію</a></li>
            <?php endif; ?>

        </ul>
    </nav>

    <section id='tours' class="panel panel-default">
        <div class="container-fluid content">
            <h1><?php echo $title; ?></h1>
            <div><?php echo $content; ?></div>
        </div>
    </section>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>
