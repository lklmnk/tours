<form method='post' action='/admin/tour/submit' enctype="multipart/form-data">
    <div>
        <input type='text' readonly="readonly" name='tour_id' value='<?php echo $tour->tour_id; ?>'>
    </div>
    <div>
    <input type='text' name='tour_name' value='<?php echo $tour->tour_name; ?>'>
    </div>
    
    <div>
    <select name='category_id'>
        <?php foreach($categories as $category): ?>
           <?php if ($tour->category_id == $category['category_id']): ?>
           <option value="<?= $category['category_id']?>" selected='selected'><?= $category['category_name']?></option>
           <?php else: ?>
           <option value="<?= $category['category_id']?>"><?= $category['category_name']?></option>
           <?php endif; ?>
        <?php endforeach; ?>
    </select>
    </div>
    <div class="form-group">
    <textarea class="form-control" name='tour_description'><?php echo $tour->tour_description; ?></textarea>
    </div>
    <div>
        <?php if ($tour->is_active): ?>
        <input type='checkbox' name='is_active' value="1" checked="checked">
        <?php else: ?>
        <input type='checkbox' name='is_active' value="1">
        <?php endif; ?>
    </div>
    <div>
    <input type='text' name='meta_description' value='<?php echo $tour->meta_description; ?>' placeholder='meta_description'>
    </div>
    <div>
    <input type='text' name='meta_keywords' value='<?php echo $tour->meta_keywords; ?>' placeholder='meta_keywords'>
    </div>
    <div>
        <input type='text' readonly="readonly" name='image_id' value='<?php echo $image->image_id; ?>' placeholder='img_id'>
    </div>
    <div>
        <input type='text' name='title' value='<?php echo $image->title; ?>' placeholder='img_title'>
    </div>
    <div>
        <input type='text' readonly="readonly" name='gallery_id' value='<?php echo $image->gallery_id; ?>' placeholder='galery_id'>
    </div>
    <input type="file" multiple="multiple" name="images[]">

    <div>
        <input type='hidden' name='action' value='<?php echo $action; ?>'>
        <input type='submit'>
    </div>
</form>