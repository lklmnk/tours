<form action="/admin/tour/submit" method="post">
<?php foreach($tours as $key => $tour): ?>
<div>
    <input type="checkbox" name="tour[]" value="<?php echo $tour['tour_id']; ?>">
    <a href="/admin/tour/edit/<?php echo $tour['tour_id']; ?>">
        <span <?php if (!$tour['is_active']): ?>style="text-decoration: line-through;"<?php endif; ?>>
           <?php echo $tour['tour_name']; ?>
        </span>
    </a>
</div>
<?php endforeach;?>
<input type="hidden" name="action" value="active">
<input type="submit"  value="Де/Актувати">
</form>
