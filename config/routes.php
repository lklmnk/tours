<?php
return array(
    ""                  => "home/index",
    "page404"           => "home/page404",
    "categories"        => "category/list",
    "category/([0-9]+)" => "category/view/$1",
    "tours"             => "tour/list",
    "tours/([0-9]+)"    => "tour/list/$1",
    "tour/([0-9]+)"     => "tour/view/$1",

);
