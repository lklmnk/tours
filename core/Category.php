<?php

class Category extends Model
{
    public $category_id = 0;
    public $parent_id=0;
    public $is_active=1;
    public $is_main=0;
    public $category_name = "";
    public $category_description = "";
    public $category_long_description = "";
    public $category_url = "";
    public $sort_order;
    public $background_image = "";
    public $background_image_header = "";





    public function updateCategoryById()
    {
        return $this->update('categories', 'category_id');
    }

    public function createCategory()
    {
        return $this->insert('categories');
    }

}
