<?php
class FrontController {
    
    protected static $path;
    protected static $mainView;
    
    public static function Init(){
        session_start();
        self::$mainView = new View(ROOT."/modules/views/index.tpl");
    }

    public static function Route(){
        self::$path = Router::Route();
    }
    
    public static function Action() {
        $parts = explode("/", self::$path);
        $module = array_shift($parts);
        $method = array_shift($parts);
        $parameters = $parts;
        $controller = ucfirst($module) . "Controller";
        $action = ucfirst($method) . "Action";
        if(class_exists($controller)){
            $controllerObject = new $controller;
            if (method_exists($controllerObject, $action)){
                $controllerObject -> $action($parameters);
            }
        }
    }
    
    public static function Display() {

         self::$mainView->Display();
    }
    
}
