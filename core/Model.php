<?php

/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 04.06.2017
 * Time: 7:56
 */
class Model
{
    public function __construct($modelName, $model, $model_id = 0)
    {
        if ($model_id > 0) {
            $models = $this->getModels($modelName,$model,$model_id);
            $model=$models[0];
            if(empty($model)){
                return 0;
            }
            foreach ($model as $property => $value) {
                if (property_exists($this, $property)) $this->$property = $value;
            }
        }

    }

    public function update($table, $fieldWhere)
    {
        $set = '';
        $values = [];
        $fields=[];
        foreach ($this as $propertyName => $propertyValue) {
            if(!empty($propertyValue)) {
                $values[] = $propertyValue;
                $fields[] = $propertyName . '=?';
            }
        }

        $set = join(', ', $fields);
        $sql = 'UPDATE ' . $table . ' SET ' . $set . ' WHERE ' . $fieldWhere . ' = ' . $this->$fieldWhere;
        $stmt = DataBase::handler()->prepare($sql);
        $stmt->execute($values);
        return $this->$fieldWhere;
    }
    public function insert($table)
    {
        $set = '';
        $values = [];
        $fields=[];
        foreach ($this as $propertyName => $propertyValue) {
            if(!empty($propertyValue)){
                $values[] = (gettype($propertyValue) == 'string')? " ' ". $propertyValue. " ' " : $propertyValue ;
                $fields[] =$propertyName;
            }
        }

        $set = join(', ', $fields);
        $valuesins = join(', ', $values);
        $stmt = DataBase::handler()->query('insert into ' . $table . '( ' . $set . ') values ( ' . $valuesins . ')');
        return DataBase::handler()->lastInsertId();
    }

    public function initObjectFromArray($array)
    {
        foreach ($array as $property => $value) {
            if (property_exists($this, $property)) $this->$property = $value;
        }
    }

    public function getModels($modelName,$model,$id=0)
    {
        $result = array();
        $where = '';
        if ($id != 0) {
            $model_id=$model.'_id';
            $where = "WHERE $model_id = " . (int)$id;
        }
        $st = DataBase::handler()->query("SELECT * FROM $modelName $where;");
        $result = $st->fetchAll();
        return $result;
    }

    public static function changeActiveStatus($array)
    {
        if (isset($_POST['category']) && !empty($_POST['category'])) {
            $models =$_POST['category'];
            $table="categories";
            $model_id="category_id";
        }
        elseif (isset($_POST['tour']) && !empty($_POST['tour'])) {
            $models =$_POST['tour'];
            $table = "tours";
            $model_id="tour_id";
        }
        else
            $models='';
        if(!empty($models)){
            $str = join(", ", $models);
            $sql = "UPDATE $table SET is_active=(case when is_active=1 then 0 else 1 end) WHERE $model_id IN (" . $str . ")";
            $stmt = DataBase::handler()->prepare($sql);
            $stmt->execute();
        }
    }
}