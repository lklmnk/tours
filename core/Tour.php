<?php

class Tour extends Model
{
    public $tour_id = 0;
    public $sort_order=0;
    public $is_active=0;
    public $tour_name = "";
    public $tour_description = "";
    public $tour_info="";
    public $tour_price_1=0;
    public $category_id = 0;
    public $tour_length_id=0;
    public $meta_description = "";
    public $meta_keywords = "";
    


    public function updateTourById()
    {
        return $this->update('tours', 'tour_id');
    }
    
    public function createTour()
    {
        return $this->insert('tours');
    }


}
