<?php

    define("ROOT", __DIR__);
    
    include(ROOT . "/config/config.php");
    
    spl_autoload_register(function($className){
        $dirs = array(
            ROOT . "/core/",
            ROOT . "/modules/controllers/",
            ROOT . "/modules/models/",
        );
        foreach($dirs as $dir) {
            if (is_file($dir . $className . ".php")) {
                include $dir . $className . ".php";
            }
        }
    });
    
    DataBase::Connect();
    FrontController::Init();
    FrontController::Route();
    FrontController::Action();
    FrontController::Display();