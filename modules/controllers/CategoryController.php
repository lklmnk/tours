<?php

class CategoryController extends FrontController {
    
    public function ViewAction($parameters)
    {
        $category_id = $parameters[0];
        $category = new Category("Categories","category",$category_id);
        $category->tours = Tour::getModels("tours","category",$category_id);
        $params["category"] = $category;
        $categories=CategoryModel::getCategories();
        $content = View::GetContents(ROOT . "/modules/views/category/view.tpl", $params);
        self::$mainView->addParam("title", $category->category_name);
        self::$mainView->addParam("content", $content);
        self::$mainView->addParam("categories",$categories);
    }

    public function ListAction($parameters)
    {
        $categories = CategoryModel::getCategories();
        $params_1 = array();
        $params_1["categories"] = $categories;
        $content_categories = View::GetContents(ROOT . "/modules/views/category/list.tpl", $params_1);
        $content = View::GetContents(ROOT . "/modules/views/category/galery.tpl", $params_1);
        self::$mainView->addParam("content_categories", $content_categories);
        self::$mainView->addParam("categories", $categories);
        self::$mainView->addParam("title", "Список категорій");
        self::$mainView->addParam("content", $content);
    }    
}
