<?php
class HomeController extends FrontController
{
    public function IndexAction($parameters)
    {
        $categories = CategoryModel::getCategories();
        $params_1 = array();
        $params_1["categories"] = $categories;
        $content_categories = View::GetContents(ROOT . "/modules/views/category/list.tpl", $params_1);
        $content = View::GetContents(ROOT . "/modules/views/category/galerycategory.tpl", $params_1);
        self::$mainView->addParam("content_categories", $content_categories);
        self::$mainView->addParam("categories", $categories);
        self::$mainView->addParam("title", "Наші тури");
        self::$mainView->addParam("content", $content);
    }
    
    public function Page404Action($parameters){
        self::$mainView->addParam("title", "Сторінка 404");
        self::$mainView->addParam("content", "Тут буде шаблон сторінки 404");
    }
}
