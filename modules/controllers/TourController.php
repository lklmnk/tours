<?php

class TourController extends FrontController {
    

    public function ListAction($parameters)
    {
        $category_id = (isset($parameters[0]))?$parameters[0]:0;
        $tours = TourModel::getModels("tours","category",$category_id);
        $category=CategoryModel::getModels("categories","category",$category_id);
        $param["category_name"]=($category_id!=0)?$category[0]["category_name"]:'';
        $params["tours"] = $tours;
        $title=($category_id==0)?"Список турів":$category[0]["category_name"];
        $categories = CategoryModel::getCategories();
        $content = View::Getcontents(ROOT . "/modules/views/tour/list.tpl", $params);
        $breadcrumb = View::Getcontents(ROOT . "/modules/views/category/breadcrumb.tpl", $param);
        self::$mainView->addParam("title", $title);
        self::$mainView->addParam("content", $content);
        self::$mainView->addParam("categories", $categories);
        self::$mainView->addParam("breadcrumb",$breadcrumb);
            }
    public function ViewAction($parameters)
    {
        $tour_id = $parameters[0];
        $tour = new Tour("tours","tour",$tour_id);
        $category_id=$tour->category_id;
        $objcategory=new Category("categories","category",$category_id);
        $tour->category_name = $objcategory->category_name;
        $params["tours"] = $tour;
        $img=Images::getModels($tour_id);
        $params_i["images"]=$img;
        $categories=CategoryModel::getCategories();
        $gallerytour = View::Getcontents(ROOT."/modules/views/tour/galerytour.tpl", $params_i);
        $breadcrumb = View::Getcontents(ROOT."/modules/views/tour/breadcrumb.tpl", $params);
        $content = View::Getcontents(ROOT."/modules/views/tour/view.tpl", $params);
        self::$mainView->addParam("title", $tour->tour_name);
        self::$mainView->addParam("content", $content);
        self::$mainView->addParam("categories",$categories);
        self::$mainView->addParam("breadcrumb",$breadcrumb);
        self::$mainView->addParam("gallerytour",$gallerytour);
    }

}
