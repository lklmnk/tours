<?php

class CategoryModel extends Category
{
    /**
     * Отримання списку категорій
    */
    public static function getCategories()
    {
        $st = DataBase::handler()->query("SELECT * FROM categories;");
        $result = $st->fetchAll();
        $category=[];
        $subcategory=[];
        foreach ($result as $res) {

            if ($res["parent_id"] == 0)
                $category[$res["category_id"]]["parent"] = $res;
            else{
                $category[(int)$res["parent_id"]]["sub"][$res["category_id"]]=$res;
                 }
        }

        return $category;
    }
}
