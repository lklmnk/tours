<?php $i=0; foreach($categories as $category): ?>
<?php if($category["is_active"]=1): ?>
    <div id="carousel<?php echo $i;?>" class="carousel slide col-xs-4 " >
        <ol class="carousel-indicators">
            <li class="active" data-target="#carousel<?php echo $i;?> col-xs-3" data-slide="0"></li>
            <li data-target="#carousel<?php echo $i;?>" data-slide="1"></li>
            <li data-target="#carousel<?php echo $i;?>" data-slide="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="item active">
                <img class="image-responsive" src="/images/05.jpg" />
                <div class="carousel-caption">
                    <h4><?php echo $category['parent']['category_name']; ?></h4>
                </div>
            </div>

            <div class="item">
                <img src="/images/06.jpg" />
                <div class="carousel-caption">
                    <h3>slaid2</h3>
                </div>
            </div>
        </div>
            <a href="#carousel<?php echo $i;?>" class="left carousel-control" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        </a>
        <a href="#carousel<?php echo $i;?>" class="right carousel-control" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        </a>

    </div>
<?php endif; $i++; ?>
<?php endforeach; ?>


