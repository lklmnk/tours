<div class="list-group">
    <span class="list-group-item active">Наші тури</span>
    <?php foreach($categories as $key => $category): ?>
    <li class="list-group-item dropdown">
        <?php if(!empty($category['sub'])): $sub=$category['sub']; ?>
        <a data-toggle="dropdown" class="dropdown-toggle " href="/category/<?php echo $category['parent']['category_id']; ?>">
            <span><?php echo $category["parent"]["category_name"].">"; ?></span>
        </a>
        <ul class="dropdown-menu">
            <?php foreach($sub as $subcategory): ?>
            <li><a href= "/category/<?php echo $subcategory['category_id']; ?>"><?php echo $subcategory["category_name"]; ?></li>
            <?php endforeach;?>
        </ul>
    </li>
    <?php else:  ?>
    <a  href="/category/<?php echo $category['parent']['category_id']; ?>">
        <span><?php echo $category["parent"]["category_name"]; ?></span>
    </a>
        <?php endif; ?>
    <?php endforeach;?>
</div>