<div class="content">
    <div >
        <?php echo $category->category_long_description; ?>
    </div>
    <div class="list-group">
        <div class="list-group-item active">Тури</div>
        <?php foreach ($category->tours as $tour): ?>
        <div class="list-group-item">
            <a href="/tour/<?php echo $tour['tour_id']; ?>"><?php echo $tour['tour_name']; ?></a>

        </div>
        <?php endforeach; ?>
    </div>
</div>

