
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?php echo $title; ?></title>
    <link rel="shortcut icon" href="/images/logo.png">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <link rel="stylesheet" href="/modules/views/css/style.css">

</head>
<body>
<div class="container">
    <img id="logo"  src="/images/logo.png">
    <nav id="nav" class="navbar navbar-default ">
        <div class="container-fluid ">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-right" id="navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li><a href="/">Головна сторінка <span class="sr-only">(current)</span></a></li>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Наші тури <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <?php foreach($categories as $key => $category): ?>
                            <?php if(empty($category['sub'])): ?>
                            <li><a  href="/category/<?php echo $category['parent']['category_id']; ?>">
                                    <span><?php echo $category["parent"]["category_name"]; ?></span>
                                </a>
                            </li>
                            <?php endif; ?>
                            <?php endforeach;?>
                        </ul>
                    </li>
                    <?php foreach($categories as $key => $category): ?>
                    <?php if(!empty($category['sub'])): $sub=$category['sub']; ?>
                    <li class=" dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                            <span><?php echo $category["parent"]["category_name"]; ?><span class="caret"></span></span>
                        </a>
                        <ul class="dropdown-menu">
                            <?php foreach($sub as $subcategory): ?>
                            <li><a href= "/category/<?php echo $subcategory['category_id']; ?>"><?php echo $subcategory["category_name"]; ?></li>
                            <?php endforeach;?>
                        </ul>
                    </li>
                    <?php endif; ?>
                    <?php endforeach;?>
                </ul>
            </div>
        </div>
    </nav>
    <?php if(isset($breadcrumb)): ?>
    <div><?php echo $breadcrumb; ?></div>
    <?php endif; ?>
    <div class="container">
        <div class="row">
            <?php if(!empty($content_categories)): ?>
            <div class="container col-xs-12 col-sm-4 col-md-3 col-lg-3">
                <div><?php echo $content_categories; ?></div>
            </div>
            <div class="col-xs-12 col-sm-8 col-md-9 col-lg-9">

                <section id='articles' class="panel panel-default">
                    <div class='container-fluid'>
                        <h1><?php echo $title; ?></h1>
                        <div><?php echo $content; ?></div>
                    </div>

                </section>
            </div>
            <?php else: ?>
            <div class='container-fluid'>
                <h1><?php echo $title; ?></h1>
                <div><?php echo $content; ?></div>
            </div>
            <?php endif; ?>
            <?php if(isset($gallerytour)): ?>
            <div ><?php echo $gallerytour; ?></div>
            <?php endif; ?>
        </div>
    </div>
</div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>
