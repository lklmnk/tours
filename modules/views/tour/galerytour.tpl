<div id="carouseltour" class="carousel slide col-xs-8 " >
    <ol class="carousel-indicators">
        <li class="active" data-target="#carouseltour" data-slide="0"></li>
        <?php  for($i=1;$i<=count($images);$i++): ?>
        <li data-target="#carouseltour" data-slide="<?php echo $i; ?>"></li>
        <?php endfor; ?>
    </ol>
    <div class="carousel-inner">
        <div class="item active">
            <img  class="img-responsive" src="/images/galleries/gallery_<?php echo $images[0]['gallery_id']; ?>/<?php echo $images[0]['image']; ?>" />
            <div class="carousel-caption">
                <h4><?php echo $images[0]['title']; ?></h4>
            </div>
        </div>
        <?php array_shift($images); foreach($images as $img): ?>
        <div class="item">
            <img  src="/images/galleries/gallery_<?php echo $img['gallery_id']; ?>/<?php echo $img['image']; ?>" />
            <div class="carousel-caption">
                <h3><?php echo $img['title']; ?></h3>
            </div>
        </div>
        <?php endforeach; ?>
    </div>
        <a href="#carouseltour" class="left carousel-control" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        </a>
        <a href="#carouseltour" class="right carousel-control" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        </a>

</div>



